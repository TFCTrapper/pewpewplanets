﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : Singleton<MainMenu>
{
    public void ResumeGame()
    {
        CrossScene.Instance.isLoadedFromSave = true;
        SceneManager.LoadScene("Game");
    }

    public void StartGame()
    {
        CrossScene.Instance.isLoadedFromSave = false;
        SceneManager.LoadScene("Game");
    }

    public void Close()
    {
        Application.Quit();
    }

    private void Awake()
    {
        UIController.Instance.Initialize();
    }
}
