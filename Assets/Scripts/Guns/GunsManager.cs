﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunsManager : Singleton<GunsManager>
{
    public GameObject[] gunPrefabs;

    [SerializeField]
    private string _gunPrefabsPath;

    public void Initialize()
    {
        UpdateGunPrefabs();
    }

    public Gun GetGun(int index)
    {
        return Instantiate(gunPrefabs[index]).GetComponent<Gun>();
    }

    public Gun GetRandomGun()
    {
        int index = Random.Range(0, gunPrefabs.Length);
        return GetGun(index);
    }

    private void UpdateGunPrefabs()
    {
        gunPrefabs = Resources.LoadAll<GameObject>(_gunPrefabsPath);
    }
}
