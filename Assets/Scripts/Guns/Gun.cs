﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public Vector3 target { get; set; }

    public Planet planet { get; private set; }

    [SerializeField]
    protected GameObject _rocketPrefab;
    [SerializeField]
    protected Transform _modelTransform;
    [SerializeField]
    protected float _cooldown;

    protected Transform _transform;

    protected float _lastFireTime;

    public virtual void Initialize(Planet planet)
    {
        _transform = GetComponent<Transform>();
        this.planet = planet;
        _transform.position = planet.transform.position;
        _transform.rotation = planet.transform.rotation;
        _transform.parent = planet.transform;
    }

    public virtual void Shoot()
    {
        if (Time.time - _lastFireTime < _cooldown)
        {
            return;
        }

        Rocket rocket = Pools.Instance.Get(_rocketPrefab, planet.transform.position, Quaternion.identity).GetComponent<Rocket>();
        rocket.Initialize(planet, target);

        _lastFireTime = Time.time;
    }

    private void Update()
    {
        _transform.LookAt(target);
        UpdateCooldownSlider();
    }

    private void UpdateCooldownSlider()
    {
        float cooldownSliderValue = Mathf.Clamp((Time.time - _lastFireTime) / _cooldown, 0f, 1f);
        UIController.Instance.cooldownSlidersManager.UpdateValue(planet, cooldownSliderValue);
    }
}
