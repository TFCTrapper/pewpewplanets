﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetsManager : Singleton<PlanetsManager>
{
    public GameObject orbitAndPlanetPrefab;
    public List<Planet> planets;
    public ActiveShootingController player;

    [SerializeField]
    private Material _playerPlanetMaterial;

    private Transform _transform;

    public void Initialize(int playersCount)
    {
        _transform = GetComponent<Transform>();

        if (CrossScene.Instance.isLoadedFromSave)
        {
            playersCount = PlayerPrefs.GetInt("PlayersCount");
        }

        int playerIndex = CrossScene.Instance.isLoadedFromSave ? PlayerPrefs.GetInt("PlayerIndex") : Random.Range(0, playersCount);

        for (int i = 0; i < playersCount; i++)
        {
            Planet planet = Instantiate(orbitAndPlanetPrefab, Vector3.zero, Quaternion.identity, _transform).GetComponentInChildren<Planet>();
            planet.Initialize(CrossScene.Instance.isLoadedFromSave, i);
            ActiveShootingController activeShootingController = i.Equals(playerIndex) ? SetPlayer(planet, out player) : SetAI(planet);
            activeShootingController.Initialize();
            planets.Add(planet);
        }
    }

    public void KillPlanet(Planet planet)
    {
        bool isPlayerKilled = (planet.GetComponent<PlayerController>() != null);

        planets.Remove(planet);
        Destroy(planet.orbit.gameObject);
        Game.Instance.CheckResult(isPlayerKilled);
    }

    public void Save()
    {
        PlayerPrefs.SetInt("PlayersCount", planets.Count);
        PlayerPrefs.SetInt("PlayerIndex", GetPlayerIndex());
        for (int i = 0; i < planets.Count; i++)
        {
            planets[i].Save(i);
        }
    }

    private ActiveShootingController SetPlayer(Planet planet, out ActiveShootingController player)
    {
        player = planet.gameObject.AddComponent<PlayerController>();
        planet.GetComponent<Renderer>().material = _playerPlanetMaterial;
        return player;
    }

    private ActiveShootingController SetAI(Planet planet)
    {
        return planet.gameObject.AddComponent<AIController>();
    }

    private int GetPlayerIndex()
    {
        for (int i = 0; i < planets.Count; i++)
        {
            if (planets[i].GetComponent<PlayerController>() != null)
            {
                return i;
            }
        }

        return -1;
    }
}
