﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour, IKillable, IDamagable, IShooting
{
    public new Transform transform { get; private set; }
    public Gun gun { get; set; }
    public float hp { get; set; }

    public float gravityMultiplier;
    public Orbit orbit;

    [SerializeField]
    private bool _randomizeParams;

    [Header("Non-Random Params")]
    [SerializeField]
    private float _speed;
    
    private int _destinationIndex;
    private Vector3 _destination;
    private Vector3 _direction;

    private int _gunIndex;

    public void Kill()
    {
        UIController.Instance.hpSlidersManager.RemoveSlider(this);
        UIController.Instance.cooldownSlidersManager.RemoveSlider(this);
        PlanetsManager.Instance.KillPlanet(this);
    }

    public void Damage(float damage)
    {
        hp -= damage;
        UIController.Instance.hpSlidersManager.UpdateValue(this);
        if (hp <= 0)
        {
            Kill();
        }
    }

    public void Shoot()
    {
        gun.Shoot();
    }

    public virtual void Initialize(bool isLoadedFromSave = false, int indexInSave = -1)
    {
        transform = GetComponent<Transform>();

        if (isLoadedFromSave)
        {
            Load(indexInSave);
        }
        else
        {
            if (_randomizeParams)
            {
                RandomizeParams();
            }
        }

        orbit.Initialize(indexInSave);
        _destinationIndex = isLoadedFromSave ?
            PlayerPrefs.GetInt("Planet" + indexInSave + "DestinationIndex") :
            Random.Range(0, orbit.pointsCount);

        if (!isLoadedFromSave)
        {
            transform.position = orbit.GetPoint(_destinationIndex);
        }

        SetNextDestinationAndDirection();

        hp = isLoadedFromSave ?
            PlayerPrefs.GetFloat("Planet" + indexInSave + "HP") : Game.Instance.maxHp;

        _gunIndex = isLoadedFromSave ?
            PlayerPrefs.GetInt("Planet" + indexInSave + "Gun") :
            Random.Range(0, GunsManager.Instance.gunPrefabs.Length);
        gun = GunsManager.Instance.GetGun(_gunIndex);
        gun.Initialize(this);

        UIController.Instance.hpSlidersManager.AddSlider(this);
        UIController.Instance.cooldownSlidersManager.AddSlider(this);
        UIController.Instance.hpSlidersManager.UpdateValue(this);
    }

    public void Save(int index)
    {
        orbit.Save(index);
        PlayerPrefs.SetInt("Planet" + index + "Gun", _gunIndex);
        PlayerPrefs.SetFloat("Planet" + index + "Scale", transform.lossyScale.x);
        PlayerPrefs.SetFloat("Planet" + index + "PositionX", transform.position.x);
        PlayerPrefs.SetFloat("Planet" + index + "PositionZ", transform.position.z);
        PlayerPrefs.SetFloat("Planet" + index + "HP", hp);
        PlayerPrefs.SetFloat("Planet" + index + "GravityMultiplier", gravityMultiplier);
        PlayerPrefs.SetFloat("Planet" + index + "Speed", _speed);
        PlayerPrefs.SetInt("Planet" + index + "DestinationIndex", _destinationIndex);
    }

    private void Update()
    {
        Move();
    }

    private void RandomizeParams()
    {
        _speed = Random.Range(Game.Instance.minMaxPlanetSpeed.x, Game.Instance.minMaxPlanetSpeed.y);
        
        float scale = Random.Range(Game.Instance.minMaxPlanetScale.x, Game.Instance.minMaxPlanetScale.y);
        transform.localScale = Vector3.one * scale;
    }

    private void SetNextDestinationAndDirection()
    {
        _destinationIndex++;
        if (_destinationIndex >= orbit.pointsCount)
        {
            _destinationIndex = 0;
        }
        _destination = orbit.GetPoint(_destinationIndex);
        _direction = (_destination - transform.position).normalized;
    }

    private void Move()
    {
        transform.position += _direction * _speed * Time.deltaTime;
        if ((_destination - transform.position).sqrMagnitude <= Mathf.Pow(2f * _speed * Time.deltaTime, 2))
        {
            SetNextDestinationAndDirection();
        }
    }

    private void Load(int indexInSave)
    {
        _speed = PlayerPrefs.GetFloat("Planet" + indexInSave + "Speed");
        float scale = PlayerPrefs.GetFloat("Planet" + indexInSave + "Scale");
        transform.localScale = Vector3.one * scale;

        float x = PlayerPrefs.GetFloat("Planet" + indexInSave + "PositionX");
        float z = PlayerPrefs.GetFloat("Planet" + indexInSave + "PositionZ");
        transform.position = new Vector3(x, 0f, z);
    }
}
