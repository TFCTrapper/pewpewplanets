﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit : MonoBehaviour
{
    public int pointsCount;

    [SerializeField]
    private bool _randomizeParams;

    [Header("Non-Random Params")]
    [SerializeField]
    private Vector2 _radiuses;
    [SerializeField]
    private float _yAngle;

    private LineRenderer _lineRenderer;
    private List<Vector3> _points = new List<Vector3>();

    public void Initialize(int indexInSave = -1)
    {
        _lineRenderer = GetComponent<LineRenderer>();

        if (CrossScene.Instance.isLoadedFromSave)
        {
            Load(indexInSave);
        }
        else
        {
            if (_randomizeParams)
            {
                RandomizeParams();
            }
        }

        UpdatePoints();
        Show();
    }

    public Vector3 GetPoint(int index)
    {
        return _points[index];
    }

    public void Save(int index)
    {
        PlayerPrefs.SetInt("Orbit" + index + "PointsCount", pointsCount);
        PlayerPrefs.SetFloat("Orbit" + index + "RadiusX", _radiuses.x);
        PlayerPrefs.SetFloat("Orbit" + index + "RadiusY", _radiuses.y);
        PlayerPrefs.SetFloat("Orbit" + index + "YAngle", _yAngle);
    }

    private void RandomizeParams()
    {
        float radiusX = Random.Range(Game.Instance.minOrbitRadiuses.x, Game.Instance.maxOrbitRadiuses.x);
        float radiusY = Random.Range(Game.Instance.minOrbitRadiuses.y, Game.Instance.maxOrbitRadiuses.y);
        _radiuses = new Vector2(radiusX, radiusY);

        _yAngle = Random.Range(0f, 360f);

        float ratio = radiusX / radiusY;
        if (ratio < 1)
        {
            ratio = 1f / ratio;
        }
        pointsCount = Mathf.RoundToInt(ratio * Game.Instance.orbitPointsCountRatio);
    }

    private void UpdatePoints()
    {
        Transform parent = Instantiate(Game.Instance.emptyPrefab, Vector3.zero, Quaternion.identity).GetComponent<Transform>();
        List<Transform> pointTransforms = new List<Transform>();

        float discreteAngle = 2f * Mathf.PI / pointsCount;
        float angle = 0;
        for (int i = 0; i < pointsCount; i++)
        {
            Vector3 point = GetPointOnOrbit(angle);
            Transform pointTransform = Instantiate(Game.Instance.emptyPrefab, point, Quaternion.identity, parent).GetComponent<Transform>();
            pointTransforms.Add(pointTransform);

            angle += discreteAngle;
        }
        parent.Rotate(Vector3.up, _yAngle);

        foreach (var pointTransform in pointTransforms)
        {
            _points.Add(pointTransform.position);
            Destroy(pointTransform.gameObject);
        }
        Destroy(parent.gameObject);
        pointTransforms.Clear();
    }

    private Vector3 GetPointOnOrbit(float angle)
    {
        float sin = Mathf.Sin(angle);
        float cos = Mathf.Cos(angle);

        float r = _radiuses.x * _radiuses.y / Mathf.Sqrt(
            Mathf.Pow(_radiuses.y * cos, 2) +
            Mathf.Pow(_radiuses.x * sin, 2)
            );

        return new Vector3(r * sin, 0f, r * cos);
    }

    private void Show()
    {
        _lineRenderer.positionCount = _points.Count;
        _lineRenderer.SetPositions(_points.ToArray());
    }

    private void Load(int indexInSave)
    {
        float radiusX = PlayerPrefs.GetFloat("Orbit" + indexInSave + "RadiusX");
        float radiusY = PlayerPrefs.GetFloat("Orbit" + indexInSave + "RadiusY");
        _radiuses = new Vector2(radiusX, radiusY);
        _yAngle = PlayerPrefs.GetFloat("Orbit" + indexInSave + "YAngle");
        pointsCount = PlayerPrefs.GetInt("Orbit" + indexInSave + "PointsCount");
    }
}
