﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossScene : Singleton<CrossScene>
{
    public bool isLoadedFromSave;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
