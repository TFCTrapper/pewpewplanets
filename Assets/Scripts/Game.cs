﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : Singleton<Game>
{
    public bool isPaused
    {
        get
        {
            return Time.deltaTime == 0f;
        }
        private set { }
    }

    public GameObject emptyPrefab;

    [Header("Orbits Random Params")]
    public Vector2 minOrbitRadiuses;
    public Vector2 maxOrbitRadiuses;
    public int orbitPointsCountRatio;

    [Header("Planet Random Params")]
    public Vector2 minMaxPlanetScale;
    public Vector2 minMaxPlanetSpeed;

    [Header("Players")]
    public int playersCount;
    public float maxHp;

    [Header("Sun")]
    public Transform sunTransform;
    public float sunGravityMultiplier;

    [Header("AI")]
    public Vector2 minMaxShootingTime;
    public float fieldSize;

    private int _result;

    public void Pause()
    {
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        Time.timeScale = 1f;
    }

    public void Restart()
    {
        PlayerPrefs.DeleteKey("Save");
        SceneManager.LoadScene("Game");
    }

    public void LoadMainMenu()
    {
        if (_result < 0)
        {
            Save();
        }
        else
        {
            DeleteSave();
        }

        Destroy(CrossScene.Instance.gameObject);
        SceneManager.LoadScene("MainMenu");
    }

    public void Close()
    {
        if (_result < 0)
        {
            Save();
        }
        else
        {
            DeleteSave();
        }

        Application.Quit();
    }

    public void CheckResult(bool isPlayerKilled)
    {
        if (isPlayerKilled)
        {
            _result = 0;
            UIController.Instance.panelsManager.Show("Result", "You lose");
        }
        else
        {
            if (PlanetsManager.instance.planets.Count == 1)
            {
                _result = 1;
                UIController.Instance.panelsManager.Show("Result", "Congratulations! You win!");
            }
        }
    }

    public void Save()
    {
        PlayerPrefs.SetInt("Save", 1);
        PlanetsManager.Instance.Save();
    }

    public void DeleteSave()
    {
        PlayerPrefs.DeleteKey("Save");
    }

    private void Awake()
    {
        _result = -1;

        Pools.Instance.Initialize();
        GunsManager.Instance.Initialize();
        UIController.Instance.Initialize();
        MousePlane.Instance.Initialize();
        PlanetsManager.Instance.Initialize(playersCount);
    }
}
