﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour, IKillable
{
    [SerializeField]
    private float _initialImpulse;
    [SerializeField]
    private float _damage;
    [SerializeField]
    private Planet _planet;
    [SerializeField]
    private float _lifeTime;

    private Rigidbody _rigidbody;
    private float _creationTime;

    public void Initialize(Planet planet, Vector3 target)
    {
        _rigidbody = GetComponent<Rigidbody>();
        _planet = planet;

        Vector3 direction = (target - _planet.transform.position).normalized;

        transform.LookAt(target);
        transform.Translate(Vector3.forward * _planet.transform.lossyScale.x * 1.5f);

        _rigidbody.AddForce(direction * _initialImpulse, ForceMode.Impulse);

        _creationTime = Time.time;
    }

    public void Kill()
    {
        Pools.Instance.Destroy(gameObject);
    }

    private void Update()
    {
        if (Time.time > _creationTime + _lifeTime)
        {
            Kill();
        }
    }

    private void FixedUpdate()
    {
        Vector3 direction = Vector3.zero;
        foreach (var planet in PlanetsManager.Instance.planets)
        {
            direction = planet.transform.position - transform.position;
            _rigidbody.AddForce(direction.normalized * planet.gravityMultiplier / direction.sqrMagnitude, ForceMode.Force);
        }

        direction = Game.Instance.sunTransform.position - transform.position;
        float force = Game.Instance.sunGravityMultiplier / direction.sqrMagnitude;
        _rigidbody.AddForce(direction.normalized * force, ForceMode.Force);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Planet"))
        {
            collision.gameObject.GetComponent<Planet>().Damage(_damage);
        }
        Kill();
    }
}
