﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pools : Singleton<Pools>
{
    [SerializeField]
    private int _initialCount;
    [SerializeField]
    private GameObject[] _initialPoolsPrefabs;
    [SerializeField]
    private string _parentPostfix;

    private Dictionary<string, List<GameObject>> _pools = new Dictionary<string, List<GameObject>>();

    public void Initialize()
    {
        foreach(GameObject prefab in _initialPoolsPrefabs)
        {
            CreatePool(prefab);
        }
    }

    public GameObject Get(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        if (!_pools.ContainsKey(prefab.name))
        {
            CreatePool(prefab);
        }
        
        foreach (var objectInPool in _pools[prefab.name])
        {
            if (!objectInPool.activeInHierarchy)
            {
                objectInPool.transform.position = position;
                objectInPool.transform.rotation = rotation;

                Rigidbody rigidbody = objectInPool.GetComponent<Rigidbody>();
                if (rigidbody != null)
                {
                    rigidbody.velocity = Vector3.zero;
                    rigidbody.angularVelocity = Vector3.zero;
                }

                objectInPool.SetActive(true);
                return objectInPool;
            }
        }

        Transform parent = GetPoolParent(prefab.name);

        GameObject gameObject = Instantiate(prefab, position, rotation, parent);
        gameObject.name = prefab.name;
        _pools[prefab.name].Add(gameObject);

        return gameObject;
    }

    public GameObject Get(GameObject prefab)
    {
        return Instantiate(prefab, Vector3.zero, Quaternion.identity);
    }

    public void Destroy(GameObject gameObject)
    {
        gameObject.SetActive(false);
    }

    private void CreatePool(GameObject prefab)
    {
        List<GameObject> pool = new List<GameObject>();
        Transform parent = Instantiate(Game.Instance.emptyPrefab, Vector3.zero, Quaternion.identity, transform).GetComponent<Transform>();
        parent.gameObject.name = prefab.name + _parentPostfix;

        for (int i = 0; i < _initialCount; i++)
        {
            GameObject gameObject = Instantiate(prefab, parent);
            gameObject.name = prefab.name;
            gameObject.SetActive(false);
            pool.Add(gameObject);
        }

        _pools.Add(prefab.name, pool);
    }

    private Transform GetPoolParent(string name)
    {
        if (_pools[name].Count > 0)
        {
            return _pools[name][0].transform.parent;
        }
        else
        {
            foreach (Transform poolTransform in transform)
            {
                if (poolTransform.name.Equals(name + _parentPostfix))
                {
                    return poolTransform;
                }
            }
        }

        return null;
    }
}
