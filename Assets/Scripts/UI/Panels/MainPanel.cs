﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainPanel : Panel
{
    [SerializeField]
    private Button _pauseButton;

    public override void Initialize()
    {
        _pauseButton.onClick.AddListener(OnPauseClick);
    }

    private void OnDestroy()
    {
        _pauseButton.onClick.RemoveListener(OnPauseClick);
    }

    private void OnPauseClick()
    {
        Game.Instance.Pause();
        UIController.Instance.panelsManager.Show("Pause");
    }
}
