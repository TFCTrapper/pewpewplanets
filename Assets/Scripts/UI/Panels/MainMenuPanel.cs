﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuPanel : Panel
{
    [SerializeField]
    private Button _resumeButton;
    [SerializeField]
    private Button _startButton;
    [SerializeField]
    private Button _closeButton;

    public override void Initialize()
    {
        _resumeButton.onClick.AddListener(OnResumeClick);
        _startButton.onClick.AddListener(OnStartClick);
        _closeButton.onClick.AddListener(OnCloseClick);

        _resumeButton.interactable = PlayerPrefs.HasKey("Save");
    }

    private void OnDestroy()
    {
        _resumeButton.onClick.RemoveListener(OnResumeClick);
        _startButton.onClick.RemoveListener(OnStartClick);
        _closeButton.onClick.RemoveListener(OnCloseClick);
    }

    private void OnResumeClick()
    {
        MainMenu.Instance.ResumeGame();
    }

    private void OnStartClick()
    {
        MainMenu.Instance.StartGame();
    }

    private void OnCloseClick()
    {
        MainMenu.Instance.Close();
    }
}
