﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PausePanel : Panel
{
    [SerializeField]
    private Button _resumeButton;
    [SerializeField]
    private Button _restartButton;
    [SerializeField]
    private Button _mainMenuButton;
    [SerializeField]
    private Button _closeButton;

    public override void Initialize()
    {
        _resumeButton.onClick.AddListener(OnResumeClick);
        _restartButton.onClick.AddListener(OnRestartClick);
        _mainMenuButton.onClick.AddListener(OnMainMenuClick);
        _closeButton.onClick.AddListener(OnCloseClick);
    }

    private void OnDestroy()
    {
        _resumeButton.onClick.RemoveListener(OnResumeClick);
        _restartButton.onClick.RemoveListener(OnRestartClick);
        _mainMenuButton.onClick.RemoveListener(OnMainMenuClick);
        _closeButton.onClick.RemoveListener(OnCloseClick);
    }

    private void OnResumeClick()
    {
        Game.Instance.Resume();
        UIController.Instance.panelsManager.Show("Main");
    }

    private void OnRestartClick()
    {
        Game.Instance.Resume();
        Game.Instance.Restart();
    }

    private void OnMainMenuClick()
    {
        Game.Instance.Resume();
        Game.Instance.LoadMainMenu();
    }

    private void OnCloseClick()
    {
        Game.Instance.Close();
    }
}
