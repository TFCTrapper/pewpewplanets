﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultPanel : Panel
{
    [SerializeField]
    private Text _messageText;
    [SerializeField]
    private Button _restartButton;
    [SerializeField]
    private Button _mainMenuButton;
    [SerializeField]
    private Button _closeButton;

    public override void Initialize()
    {
        _restartButton.onClick.AddListener(OnRestartClick);
        _mainMenuButton.onClick.AddListener(OnMainMenuClick);
        _closeButton.onClick.AddListener(OnCloseClick);
    }

    public override void Show(string message)
    {
        base.Show(message);
        _messageText.text = message;
    }

    private void OnDestroy()
    {
        _restartButton.onClick.RemoveListener(OnRestartClick);
        _mainMenuButton.onClick.RemoveListener(OnMainMenuClick);
        _closeButton.onClick.RemoveListener(OnCloseClick);
    }

    private void OnResumeClick()
    {
        Game.Instance.Resume();
        UIController.Instance.panelsManager.Show("Main");
    }

    private void OnRestartClick()
    {
        Game.Instance.Resume();
        Game.Instance.Restart();
    }

    private void OnMainMenuClick()
    {
        Game.Instance.Resume();
        Game.Instance.LoadMainMenu();
    }

    private void OnCloseClick()
    {
        Game.Instance.Close();
    }
}
