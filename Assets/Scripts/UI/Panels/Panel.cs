﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Panel : MonoBehaviour
{
    public virtual void Initialize()
    {

    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Show(string message)
    {
        Show();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
