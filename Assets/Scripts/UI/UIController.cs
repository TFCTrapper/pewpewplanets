﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : Singleton<UIController>
{
    public PanelsManager panelsManager;

    public HpSlidersManager hpSlidersManager;
    public CooldownSlidersManager cooldownSlidersManager;

    public void Initialize()
    {
        panelsManager.Initialize();

        if (hpSlidersManager != null)
        {
            hpSlidersManager.Initialize();
        }
        if (cooldownSlidersManager != null)
        {
            cooldownSlidersManager.Initialize();
        }
    }
}