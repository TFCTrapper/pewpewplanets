﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownSlidersManager : PlanetSlidersManager
{
    public override void UpdateValue(Planet planet, float value = -1f)
    {
        _sliders[planet].value = value;
    }
}
