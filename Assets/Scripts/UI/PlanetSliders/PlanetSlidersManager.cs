﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetSlidersManager : MonoBehaviour
{
    [SerializeField]
    protected GameObject _sliderPrefab;
    [SerializeField]
    protected float _sliderYOffset;
    [SerializeField]
    protected Vector2 _sliderAnchoredSize;
    [SerializeField]
    protected Camera _camera;

    protected Dictionary<Planet, Slider> _sliders = new Dictionary<Planet, Slider>();
    protected Dictionary<Planet, RectTransform> _slidersTransforms = new Dictionary<Planet, RectTransform>();

    public void Initialize()
    {
        _camera = Camera.main;
    }

    public void AddSlider(Planet planet)
    {
        Slider slider = Instantiate(_sliderPrefab, transform).GetComponent<Slider>();
        _sliders.Add(planet, slider);
        _slidersTransforms.Add(planet, slider.GetComponent<RectTransform>());
    }

    public void RemoveSlider(Planet planet)
    {
        GameObject sliderGameObject = _sliders[planet].gameObject;
        _sliders.Remove(planet);
        _slidersTransforms.Remove(planet);
        Destroy(sliderGameObject);
    }

    public virtual void UpdateValue(Planet planet, float value = -1f) { }

    protected void Update()
    {
        UpdateSlidersPositions();
    }

    protected void UpdateSlidersPositions()
    {
        foreach (Planet planet in _slidersTransforms.Keys)
        {
            Vector2 positionOnViewport = _camera.WorldToViewportPoint(planet.transform.position);

            _slidersTransforms[planet].anchorMin = new Vector2(
                positionOnViewport.x - _sliderAnchoredSize.x / 2f,
                positionOnViewport.y - _sliderAnchoredSize.y / 2f + _sliderYOffset
                );

            _slidersTransforms[planet].anchorMax = new Vector2(
                positionOnViewport.x + _sliderAnchoredSize.x / 2f,
                positionOnViewport.y + _sliderAnchoredSize.y / 2f + _sliderYOffset
                );
        }
    }
}
