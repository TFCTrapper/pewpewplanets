﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveShootingController : MonoBehaviour
{
    protected Transform _transform;
    protected IShooting _shooting;

    public virtual void Initialize()
    {
        _transform = GetComponent<Transform>();
        _shooting = GetComponent<IShooting>();
    }
}
