﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : ActiveShootingController
{
    public override void Initialize()
    {
        base.Initialize();
        gameObject.name = "Player";
    }

    private void Update()
    {
        if (Game.Instance.isPaused)
        {
            return;
        }

        _shooting.gun.target = MousePlane.Instance.lastMousePosition;

        if (Input.GetMouseButtonDown(0))
        {
            _shooting.Shoot();
        }
    }
}