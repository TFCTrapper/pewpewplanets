﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : ActiveShootingController
{
    [SerializeField]
    private float _shootingTime;

    private float _lastShotTime;

    public override void Initialize()
    {
        base.Initialize();
        _shootingTime = Random.Range(Game.Instance.minMaxShootingTime.x, Game.Instance.minMaxShootingTime.y);
    }

    private void Update()
    {
        TryShoot();
    }

    private void TryShoot()
    {
        if (Time.time < _lastShotTime + _shootingTime)
        {
            return;
        }

        float targetX = Random.Range(-Game.Instance.fieldSize, Game.Instance.fieldSize) / 2f;
        float targetY = Random.Range(-Game.Instance.fieldSize, Game.Instance.fieldSize) / 2f;
        _shooting.gun.target = new Vector3(targetX, 0f, targetY);
        _shooting.Shoot();
        _lastShotTime = Time.time;
    }
}
