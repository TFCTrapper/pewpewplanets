﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePlane : Singleton<MousePlane>
{
    public Vector3 lastMousePosition;
    
    [SerializeField]
    private Camera _camera;

    private Transform _transform;

    public void Initialize()
    {
        _camera = Camera.main;
        _transform = GetComponent<Transform>();
    }

    private void Update()
    {
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);

        RaycastHit[] hits = Physics.RaycastAll(ray, _camera.transform.position.y * 2f);
        foreach (var hit in hits)
        {
            if (hit.transform.Equals(_transform))
            {
                lastMousePosition = hit.point;
                break;
            }
        }
    }
}
