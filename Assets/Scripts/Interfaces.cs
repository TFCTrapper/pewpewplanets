﻿using UnityEngine;

interface IKillable
{
    void Kill();
}

interface IDamagable
{
    float hp { get; set; }
    void Damage(float damage);
}

public interface IShooting
{
    Gun gun { get; set; }
    void Shoot();
}